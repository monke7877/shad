{ config, lib, pkgs, modulesPath, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.kernelModules          = [];
  boot.initrd.availableKernelModules = [
    "ahci"
    "sd_mod"
    "usbhid"
    "usb_storage"
    "xhci_pci"
  ];

  boot.extraModulePackages = [];
  boot.kernelModules       = [ "kvm-amd" ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/4dcfe8c4-0efc-49f4-9c38-e4acf3d9b82b";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/D3A2-41CC";
    fsType = "vfat";
  };

  swapDevices = [];

  networking.useDHCP               = lib.mkDefault true;
  nixpkgs.hostPlatform             = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
