{ stateversion, pkgs, ... }: {
  imports = [
    ./hardware.nix

    ../common/services/gnupg.nix
    ../common/services/openssh.nix
    ../common/utils/doas.nix
    ../common/utils/nix.nix
    ../common/utils/unfree.nix
  ];

  environment.systemPackages = with pkgs; [ git go-task ];
  networking.hostName        = "syxx";
  system.stateVersion        = stateversion; 
}
