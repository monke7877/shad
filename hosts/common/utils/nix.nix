{ pkgs, ... }: {
  nix = {
    package = pkgs.nixFlakes;

    gc = {
      automatic = true;
      dates     = "weekly";
      options   = "--delete-older-than 15d";
    };

    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
}
