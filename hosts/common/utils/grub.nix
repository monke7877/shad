{ ... }: {
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint     = "/boot";
    };

    grub = {
      device      = "nodev";
      efiSupport  = true;
      useOSProber = true;
    };
  };
}
