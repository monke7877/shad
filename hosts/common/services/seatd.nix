{ lib, pkgs, ... }: {
  systemd.services.seatd = {
    enable = true;

    description = "seatd";
    script      = "${lib.getExe pkgs.seatd} -g wheel";
    wantedBy    = [ "multi-user.target" ];

    serviceConfig = {
      Restart    = "always";
      RestartSec = "1";
      Type       = "simple";
    };
  };
}
