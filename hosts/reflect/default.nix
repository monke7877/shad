{ stateversion, pkgs, ... }: {
  imports = [
    ./hardware.nix

    ../common/services/audio.nix
    ../common/services/bluetooth.nix
    ../common/services/gnupg.nix
    ../common/services/networking.nix
    ../common/services/openssh.nix
    ../common/services/polkit.nix
    ../common/services/seatd.nix

    ../common/utils/doas.nix
    ../common/utils/grub.nix
    ../common/utils/nix.nix
    ../common/utils/unfree.nix
  ];

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone      = "America/Sao_Paulo";
  
  boot.supportedFilesystems  = [ "ntfs" ];
  environment.systemPackages = with pkgs; [ git go-task ];
  hardware.opengl.enable     = true;
  networking.hostName        = "reflect";
  system.stateVersion        = stateversion; 
}
