{ config, lib, modulesPath, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.kernelModules          = [];
  boot.initrd.availableKernelModules = [
    "ahci"
    "sd_mod"
    "usbhid"
    "usb_storage"
    "xhci_pci"
  ];

  boot.extraModulePackages = [];
  boot.kernelModules       = [ "kvm-intel" ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/40e89c98-7b03-43f4-8bd1-ae4b0a29484e";
    fsType = "ext4";
  };

  fileSystems."/boot" =  {
    device = "/dev/disk/by-uuid/0426-4D4F";
    fsType = "vfat";
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/6740f621-8626-4893-896b-e03590ac5b9e";
    fsType = "ext4";
  };

  swapDevices = [];

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  networking.useDHCP                 = lib.mkDefault true;
  nixpkgs.hostPlatform               = lib.mkDefault "x86_64-linux";
  powerManagement.cpuFreqGovernor    = lib.mkDefault "powersave";
}
