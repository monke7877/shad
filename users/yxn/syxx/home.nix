
{ pkgs, inputs, stateversion, ... }: {
  home = {
    homeDirectory = "/home/yxn";
    stateVersion  = stateversion; 
    username      = "yxn";
  };
  
  packages = with pkgs; [
    cloudflared
  ];

  imports = [
    ../common/themes/rosepine.nix

    ../common/apps/fish.nix
    ../common/apps/git.nix
    ../common/apps/neovim.nix
    ../common/apps/tmux.nix
  ];

  programs.home-manager.enable = true;
}
