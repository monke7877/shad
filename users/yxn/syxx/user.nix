{ pkgs, ... }: {
  programs.fish.enable  = true;

  users.users.yxn = {
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCG+DpBTM2Ikl439C4YmF8kvzFn2zzgjEnhGPsCLm6HZaz3C7y1kwXFDUcpQE42OWXpudfoZ4rq/KHGkZQjW2FgMf10ax7K0A0Uhq01HZy2+P71CJLXO28cDkOI0FSILPdmlguZ+k4FY0Q2q2E64Q114wXSJX9Q0O9wjHy1Tc+QiYh8ZlwTimzyXHNV+mW7MAMyeGVQSmGHpIHTc3qMhOB5nyr8sw3tBSlOdiIBe0BjplLU1jAIZbyEFVDRQ6pBUdtv1VJys/YGkwkcZzUYdMbnqAa6MhfLoljdxxKOYsx9K+I/rOZS6bvgmi9KyKOERo/uLxkJyQ3O/SlgUvvFpPo7Jo8KDalmbs3Dn5Fu1FXV3avX82OZume5KH2/jSBBn0Kee9ZUumlQbOcvfsGOb9hOpMBW8A8Uqgy2JCuUH8n+2czxmsd/W7GD87RvKb0St3GVgwtHUKr7lskhqfwxIPjzRnCBE0suSJxdv0hsZ3Xc1yprU8lpKrTOoefVOGb/3T0= yxn@reflect" ];

    initialPassword = "yxnyxnyxn";
    isNormalUser    = true;

    extraGroups = [
      "wheel"
    ];

    shell = pkgs.fish;

    packages = with pkgs; [
      neovim
    ];
  };
}
