{ pkgs, ... }: {
  imports = [
    ../common/fonts
  ];

  programs.fish.enable = true;
  programs.steam = {
    enable = true;

    remotePlay.openFirewall = true;
  };

  users.users.yxn = {
    initialPassword = "yxnyxnyxn";
    isNormalUser    = true;

    extraGroups     = [
      "networkmanager"
      "wheel"
    ];

    shell = pkgs.fish;
  
    packages = with pkgs; [
      neovim
    ];
  };
}
