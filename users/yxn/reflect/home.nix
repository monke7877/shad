{ pkgs, inputs, stateversion, ... }: {
  home = {
    homeDirectory = "/home/yxn";
    stateVersion  = stateversion; 
    username      = "yxn";

    packages = with pkgs; [
      aria2
      imv
      lazygit
      mpv
      neofetch
      pass
      tldr

      bun
      clang
      rustup

      firefox
      godot_4
      grapejuice
      obsidian
      pamixer
      spotify

      cemu
      ryujinx
      yuzu-early-access

      discord-ptb
      telegram-desktop
      whatsapp-for-linux

      jdk11

      lunar-client
      prismlauncher

      jetbrains.idea-ultimate
      jetbrains.rust-rover
    ];
  };
  
  imports = [
    ../common/themes/rosepine.nix

    ../common/environments/cellar

    ../common/apps/fish.nix
    ../common/apps/git.nix
    ../common/apps/kitty.nix
    ../common/apps/neovim.nix
    ../common/apps/qutebrowser.nix
    ../common/apps/tmux.nix

    ../common/services/syncthing.nix
  ];

  programs.home-manager.enable = true;
}
