{ pkgs, ... }: {
  home.packages = with pkgs; [
    eww-wayland
    playerctl
  ];

  home.file.".config/eww" = {
    recursive = true;
    source    = ./files/eww;
  };
}
