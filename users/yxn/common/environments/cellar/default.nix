{ ... }: {
  imports = [
    ./eww.nix
    ./hyprland.nix
    ./rofi.nix
    ./scripts.nix
  ];
}
