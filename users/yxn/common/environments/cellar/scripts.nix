{ pkgs, ... }: {
  home.packages = with pkgs; [
    brightnessctl
    jq
    socat
  ];

  home.file.".scripts" = {
    recursive = true;
    source    = ./files/scripts;
  };
}
