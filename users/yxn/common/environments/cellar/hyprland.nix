{ inputs, pkgs, ... }: {
  imports = [
    inputs.hyprland.homeManagerModules.default
  ];

  home.packages = with pkgs; [
    wbg
  ];

  home.file.".config/hypr" = {
    recursive = true;
    source    = ./files/hyprland;
  };

  programs.fish.loginShellInit          = ''if [ "$(tty)" = "/dev/tty1" ]; Hyprland; end'';
  wayland.windowManager.hyprland.enable = true;
}
