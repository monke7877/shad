{ pkgs, config, ... }: {
  programs.rofi = {
    enable  = true;

    plugins = with pkgs; [
      rofi-emoji
    ];

    theme = let
      inherit (config.lib.formats.rasi) mkLiteral;
    in {
      "*" = {
        background-color  = mkLiteral "#${ config.colors_16.base00 }ff";
        font              = "GohuFont 11 Nerd Font Mono 12";
      };

      "window" = {
        border-color     = mkLiteral "#${ config.colors_16.base08 }ff";
        border           = 2;
        padding          = 5;
      };
      
      "inputbar" = {
        spacing     = 0;
        padding     = 1;
        text-color  = mkLiteral "#${ config.colors_16.base05 }ff";
        children    = mkLiteral "[ prompt, prompt-colon, entry ]";
      };
      
      "prompt" = {
        spacing     = 0;
        text-color  = mkLiteral "#${ config.colors_16.base08 }ff";
      };

      "prompt-colon" = {
        str         = ":";
        margin      = mkLiteral "0px 0.3000em 0.0000em 0.0000em";
        expand      = mkLiteral "false";
        text-color  = mkLiteral "#${ config.colors_16.base08 }ff";
      };

      "entry" = {
        spacing     = 0;
        text-color  = mkLiteral "#${ config.colors_16.base05 }ff";
      };
    };
  };
}
