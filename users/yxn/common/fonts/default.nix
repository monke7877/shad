{ pkgs, ... }: {
  fonts.fontconfig.enable = true;

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Gohu" ]; })
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
  ];
}
