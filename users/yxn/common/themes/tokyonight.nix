{ lib, config, ... }: with lib; let
   hex = mkOptionType {
    name = "hex-color";
    check = x: isString x && !(hasPrefix "#" x);
  };
in {
  options.colors_16 = mkOption {
    type = with types; attrsOf (
      coercedTo str (removePrefix "#") hex
    );
    default = { };
  };

  options.colors_8 = mkOption {
    type = with types; attrsOf (
      coercedTo str (removePrefix "#") hex
    );
    default = { };
  };

  config.colors_16 = {
    base00 = "#1a1b26";
    base01 = "#16161e";
    base02 = "#2f3549";
    base03 = "#444b6a";
    base04 = "#787c99";
    base05 = "#a9b1d6";
    base06 = "#cbccd1";
    base07 = "#d5d6db";
    base08 = "#c0caf5";
    base09 = "#a9b1d6";
    base0A = "#0db9d7";
    base0B = "#9ece6a";
    base0C = "#b4f9f8";
    base0D = "#2ac3de";
    base0E = "#bb9af7";
    base0F = "#f7768e";
  };

  config.colors_8 = {
    base00 = "#15161e";
    base01 = "#f7768e";
    base02 = "#9ece6a";
    base03 = "#e0af68";
    base04 = "#7aa2f7";
    base05 = "#bb9af7";
    base06 = "#7dcfff";
    base07 = "#a9b1d6";
    base08 = "#414868";
  };
}
