{ lib, config, ... }: with lib; let
   hex = mkOptionType {
    name = "hex-color";
    check = x: isString x && !(hasPrefix "#" x);
  };
in {
  options.colors_16 = mkOption {
    type = with types; attrsOf (
      coercedTo str (removePrefix "#") hex
    );
    default = { };
  };

  options.colors_8 = mkOption {
    type = with types; attrsOf (
      coercedTo str (removePrefix "#") hex
    );
    default = { };
  };

  config.colors_16 = {
    base00 = "#191724";
    base01 = "#1f1d2e";
    base02 = "#26233a";
    base03 = "#6e6a86";
    base04 = "#908caa";
    base05 = "#e0def4";
    base06 = "#e0def4";
    base07 = "#524f67";
    base08 = "#eb6f92";
    base09 = "#f6c177";
    base0A = "#ebbcba";
    base0B = "#31748f";
    base0C = "#9ccfd8";
    base0D = "#c4a7e7";
    base0E = "#f6c177";
    base0F = "#524f67";
  };

  config.colors_8 = {
    base00 = "#191724";
    base01 = "#eb6f92";
    base02 = "#31748f";
    base03 = "#f6c177";
    base04 = "#9ccfd8";
    base05 = "#c4a7e7";
    base06 = "#ebbcba";
    base07 = "#e0def4";
    base08 = "#6e6a86";
  };
}
