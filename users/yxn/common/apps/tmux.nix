{ pkgs, config, ... }: {
  programs.tmux = {
    enable = true;
    
    shell    = "${pkgs.fish}/bin/fish";
    terminal = "tmux-256color";

    baseIndex    = 1;
    escapeTime   = 0;
    historyLimit = 10000;
    keyMode      = "vi";
    newSession   = true;
    shortcut     = "space";

    extraConfig = ''
      bind -n M-h select-pane -L
      bind -n M-j select-pane -D
      bind -n M-k select-pane -U
      bind -n M-l select-pane -R
      bind -n M-H resize-pane -L 5
      bind -n M-J resize-pane -D 5
      bind -n M-K resize-pane -U 5
      bind -n M-L resize-pane -R 5

      set-option -g status-position "bottom"
      set-option -g status-style bg=default,fg=default
      set-option -g status-justify centre
      set-option -g status-left '#[bg=default,fg=default]#{?client_prefix,,  tmux  }#[bg=#${ config.colors_8.base01 },fg=black]#{?client_prefix,  tmux  ,}'
      set-option -g status-right '#S '
      set-option -g window-status-format ' #I:#W '
      set-option -g window-status-current-format '#[bg=#${ config.colors_8.base01 },fg=black] #I:#W#{?window_zoomed_flag,  , }'
    '';
  };

  programs.fish.shellInit = "tmux new -A -s term && clear";
}
