{ pkgs, ... }: {
  home.packages = with pkgs; [
    bat
    duf
    eza
    fd
    ripgrep
  ];
  
  programs.fish = {
    enable = true;

    interactiveShellInit = ''
      set -U fish_greeting
    '';

    shellAliases = {
      cat  = "bat";
      df   = "duf";
      find = "fd";
      grep = "rg";
      ls   = "eza -la --group-directories-first";
    };
  };

  programs.starship.enable = true;
}
