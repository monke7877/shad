{ pkgs, config, ... }: {
  programs.kitty = {
    enable = true;

    settings = {
      confirm_os_window_close = 0;
      cursor_shape            = "underline";
      scrollback_lines        = 1000;
      window_margin_width     = 2;

      font_family = "GohuFont 11 Nerd Font Mono";
      font_size   = 14;

      background = "#${ config.colors_8.base00 }";
      cursor     = "#${ config.colors_8.base07 }";
      foreground = "#${ config.colors_8.base07 }";

      color0  = "#${ config.colors_8.base00 }";
      color1  = "#${ config.colors_8.base01 }";
      color2  = "#${ config.colors_8.base02 }";
      color3  = "#${ config.colors_8.base03 }";
      color4  = "#${ config.colors_8.base04 }";
      color5  = "#${ config.colors_8.base05 }";
      color6  = "#${ config.colors_8.base06 }";
      color7  = "#${ config.colors_8.base07 }";

      color8  = "#${ config.colors_8.base08 }";
      color9  = "#${ config.colors_8.base01 }";
      color10 = "#${ config.colors_8.base02 }";
      color11 = "#${ config.colors_8.base03 }";
      color12 = "#${ config.colors_8.base04 }";
      color13 = "#${ config.colors_8.base05 }";
      color14 = "#${ config.colors_8.base06 }";
      color15 = "#${ config.colors_8.base07 }";
    };
  };
}
