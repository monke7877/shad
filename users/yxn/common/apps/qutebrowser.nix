{ pkgs, ... }: {
  programs.qutebrowser = {
    enable = true;

    package = pkgs.qutebrowser.override {
      enableWideVine = true;
    };

    searchEngines = {
      DEFAULT = "https://google.com/search?hl=en&q={}";
      g       = "https://google.com/search?hl=en&q={}";
    };

    settings = {
      colors.webpage = {
        darkmode.enabled       = true;
        preferred_color_scheme = "dark";
      };

      url.default_page = "startpage.perception.ovh";
      url.start_pages  = "startpage.perception.ovh";
    };
  };
}
