{ ... }: {
  programs.git = {
    enable = true;

    userEmail = "yxnlmao@gmail.com";
    userName  = "yxn";

    extraConfig.init.defaultBranch = "master";
  };
}
