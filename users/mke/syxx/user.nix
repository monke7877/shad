{ pkgs, ... }: {
  programs.fish.enable = true;

  users.users.mke = {
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDOqw+zxDhxQPDufNJbuJG357R2kfTPNJWR63gbjKr8QKe7mx+jFzjsm2dmoFhAHWG6Eo8qL+Oc1YbErxa7/pnPUDRPU1M6/s8/7NQ/vE0LPM2oUCU3/SzWn/6+pErZWFZQ+5JH/TLllQZFJmtjqyIieE5tcO5n/+ZgPHuu+6MefpvOBQFLZcHXs5H6loWyud94zPPJNwifUhPk7YyqSZgAVqacS3Z4NOxAUBEN6rSwn4YytiW8svt8P6lLWl3IJjcvnil9ZNigcd/l9aofM0A6IzI0JAtHZMlIaKIqpDVH/Nv56NWIyfjaFcmC9mEy9zAYZjPoP1l5loEhybfVnwGd2OLgQQwrRnom0ZXsCYAeqnYU8HSHVggb0RFsSC8cbA4OdNdqhhokIK4qFJPMsIpUkjfY/Bd9JaNYBfnfOisy6cz0qSGE+q3Ia9qGZ95TPtbh7x7LT62L54pNWTHzHZn+vpgWEX85JaoyXoiDrX0ZHe74/Ps7lBSzl8fslOdV5w8= rudim@DESKTOP-BH2225F" ];

    initialPassword = "mkemkemke";
    isNormalUser    = true;

    extraGroups = [
      "wheel"
    ];

    shell = pkgs.fish;

    packages = with pkgs; [
      neovim

      bun
      clang
      rustup

      lazygit
      neofetch
    ];
  };
}

