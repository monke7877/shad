{
pkgs, inputs, stateversion, ... }: {
  home = {
    homeDirectory = "/home/mke";
    stateVersion  = stateversion; 
    username      = "mke";

    packages = with pkgs; [
      imv
      neofetch
      htop
      tmux

      bun
      grim
      unzip
      dunst

      librewolf
      microsoft-edge
      grapejuice
      pamixer
      spotify

      discord
      telegram-desktop

      jdk11
      go

      prismlauncher

      jetbrains.webstorm
      vscode
      neovide
    ];
  };
  
  imports = [
    ../../yxn/common/themes/tokyonight.nix

    ../../yxn/common/environments/cellar

    ../../yxn/common/apps/fish.nix
    ../../yxn/common/apps/kitty.nix
    ../../yxn/common/apps/neovim.nix
    ../../yxn/common/apps/qutebrowser.nix
  ];

  programs.home-manager.enable = true;
}
