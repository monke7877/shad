{ pkgs, ... }: {
  imports = [
    ../../yxn/common/fonts
  ];

  programs.fish.enable = true;
  programs.steam = {
    enable = true;

    remotePlay.openFirewall = true;
  };

  users.users.mke = {
    initialPassword = "mkemkemke";
    isNormalUser    = true;

    extraGroups     = [
      "networkmanager"
      "wheel"
    ];

    shell = pkgs.fish;
  
    packages = with pkgs; [
      neovim
    ];
  };
}
