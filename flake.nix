{
  description = "yxn's nix config";

  inputs = {
    hyprland.url = "github:hyprwm/Hyprland";
    nixpkgs.url  = "github:followin/nixpkgs/jetbrains-rust-rover-init";

    home-manager = {
      inputs.nixpkgs.follows = "nixpkgs";
      url                    = "github:nix-community/home-manager";
    };
  };

  outputs = { self, nixpkgs, hyprland, home-manager } @ inputs: let
    stateversion = "23.11";
  in {
    nixosConfigurations = {
      "mike" = nixpkgs.lib.nixosSystem {
        specialArgs = { inherit inputs stateversion; };
        system      = "x86_64-linux";

        modules = [
          ./hosts/mike 
          ./users/mke/mike/user.nix

	        home-manager.nixosModules.home-manager {
      	    home-manager.extraSpecialArgs = { inherit inputs stateversion; };
            home-manager.useGlobalPkgs    = true;
            home-manager.useUserPackages  = true;
          }
        ];
      };

      "reflect" = nixpkgs.lib.nixosSystem {
        specialArgs = { inherit inputs stateversion; };
        system      = "x86_64-linux";

        modules = [
          ./hosts/reflect 
          ./users/yxn/reflect/user.nix

	        home-manager.nixosModules.home-manager {
      	    home-manager.extraSpecialArgs = { inherit inputs stateversion; };
            home-manager.useGlobalPkgs    = true;
            home-manager.useUserPackages  = true;
          }
        ];
      };

      "syxx" = nixpkgs.lib.nixosSystem {
        specialArgs = { inherit inputs stateversion; };
        system      = "x86_64-linux";

        modules = [
          ./hosts/syxx
          ./users/mke/syxx/user.nix
          ./users/yxn/syxx/user.nix

	        home-manager.nixosModules.home-manager {
      	    home-manager.extraSpecialArgs = { inherit inputs stateversion; };
            home-manager.useGlobalPkgs    = true;
            home-manager.useUserPackages  = true;
          }
        ];
      };
    };

    homeConfigurations = {
      "mke@mike" = home-manager.lib.homeManagerConfiguration {
        extraSpecialArgs = { inherit inputs stateversion; };
        pkgs             = nixpkgs.legacyPackages.x86_64-linux;

        modules = [
          ./users/mke/mike/home.nix
        ];
      };

      "yxn@reflect" = home-manager.lib.homeManagerConfiguration {
        extraSpecialArgs = { inherit inputs stateversion; };
        pkgs             = nixpkgs.legacyPackages.x86_64-linux;

        modules = [
          ./users/yxn/reflect/home.nix
        ];
      };

      "yxn@syxx" = home-manager.lib.homeManagerConfiguration {
        extraSpecialArgs = { inherit inputs stateversion; };
        modules          = [ ./users/yxn/syxx/home.nix ];
        pkgs             = nixpkgs.legacyPackages.x86_64-linux;
      };
    };
  };
}
